class Solution:
    def postorder(self, root, result):
        if (root):
            if (root.left):
                self.postorder(root.left, result)
            if (root.right):
                self.postorder(root.right, result)
            result.append(root.val)

    def postorderTraversal(self, root):
        result = []
        self.postorder(root, result)
        return result
