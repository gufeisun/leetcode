struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x): val(x), left(NULL), right(NULL) {}
};


class Solution {
public:
    void dfs(TreeNode *root, int cur, int &res) {
        if (root->left == NULL && root->right == NULL) {
            cur = cur * 10 + root->val;
            res += cur;
        } else {
            cur = cur * 10 + root->val;
            if (root->left != NULL) {
                dfs(root->left, cur, res);
            }
            if (root->right != NULL) {
                dfs(root->right, cur, res);
            }

        }
    }
    int sumNumbers(TreeNode *root) {
        int res = 0;
        if (!root) return res;
        dfs(root, 0, res);
        return res;
    }
};
