class Solution:
    def inorder(self, root, result):
        if (root):
            if (root.left):
                self.inorder(root.left, result)
            result.append(root.val)
            if (root.right):
                self.inorder(root.right, result)

    def inorderTraversal(self, root):
        result = []
        self.inorder(root, result)
        return result
