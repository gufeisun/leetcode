

#include <iostream>
#include <string>

using namespace std;
using std::string;

class Solution {
public:
    int removeElement(int A[], int n, int elem) {
        int i = 0, j = 0;
        while(j < n) {
            if (A[j] == elem) {
                j++;
            } else {
                A[i] = A[j];
                i++;
                j++;
            }
        }
        return i;
    }

};


int main() {
    Solution s;

    int A[8] = {0, 2, 1, 5, 2, 0, 3, 2};
    int result = s.removeElement(A, 8, 2);
    std::cout << "length become :" << result << std::endl;
    std::cout << "New A is: " << std::endl;
    for (int i = 0; i < result; ++i) {
        std::cout << A[i] << std::endl;
    }
    return 0;
}

