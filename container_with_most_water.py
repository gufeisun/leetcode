class Solution:
    def maxArea(self, height):
        i = 0
        length = len(height)
        if (length <= 1):
            return 0
        j = length - 1
        max_area = min(height[i], height[j]) * (j - i)
        while (i < j):
            if (height[i] < height[j]):
                i += 1
            else:
                j -= 1
            temp = min(height[i], height[j]) * (j - i)
            if (temp > max_area):
                max_area = temp
        return max_area
