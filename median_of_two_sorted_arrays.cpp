//http://blog.csdn.net/teaspring/article/details/17260081

#include <iostream>
using namespace std;

class Solution {
public:
    bool findMedianSortedArraysCore(int A[], int m, int B[], int n, double &res, int tag) {
        int c = m, f = 0, k = 0; // c stands for ceiling and f stands for floor
        int mid = (m + n) / 2;
        while (f < c) { //[f, c)
            k = (c + f) / 2;
            int j = mid - k - 1; //B[j] should <= A[k]
            if (j < -1) {
                c = k; //reduce k to enlarge c
            } else if (j == -1) { //edge case for m - n == 1
                break;
            } else if (j > n - 1) {
                f = k + 1;
            } else if (B[j] <= A[k]) {
                if (j == n - 1 || B[j+1] >= A[k]) {
                    break;
                } else {
                    c = k;
                }
            } else {
                f = k + 1;
            }
        }

        if (f < c) {
            if (!tag) {
                int prev = B[n-1-k];
                if (k > 0 && A[k-1] > prev) {
                    prev = A[k-1];
                }
                res = (A[k] + prev) / 2;
            } else {
                res = A[k];
            }
            return true;
        } else {
            return false;
        }
    }

    double findMedianSortedArrays(int A[], int m, int B[], int n) {
        double res = 0;
        int tag = 0;
        if ((m + n) % 2 == 1)
            tag = 1;
        if (!findMedianSortedArraysCore(A, m, B, n, res, tag)) {
            findMedianSortedArraysCore(B, n, A, m, res, tag);
        }

        return res;
    }

    /* Find the median from two sorted array. Each array has n elements. */

    bool findMedianCore(int A[], int B[], int n, int &res) { //find total median in array A (contains n elements)
        int u = n, f = 0, k = 0;
        while (f < u) { //[v, u)
            k = (f + u) / 2; // search A[k] as median upper of 2n integers
            int j = n - 1 - k;
            if (j < 0) {
                u = k;
            } else if (B[j] <= A[k]) {
                if (j == n-1 || B[j+1] >= A[k]) {
                    break;
                } else {
                    u = k;
                }
            } else {
                f = k + 1;
            }
        }

        if (f < u) {
            int prev = B[n - 1 -k];
            if (k > 0 && A[k - 1] > prev) {
                prev = A[k - 1];
            }
            res = (A[k] + prev) / 2;
            return true;
        }  else {
            return false;
        }
    }

    int findMedian(int A[], int B[], int n) {
        int res = 0;
        if (!findMedianCore(A, B, n, res)) {
            findMedianCore(B, A, n, res);
        }
        return res;
    }

    /* END*/
};


int main() {
    Solution s;
    int A[] = {1, 3, 6, 8, 9};
    int B[] = {2, 4, 5, 7, 10, 11, 12};
    int m = 5, n = 7;
    cout << s.findMedianSortedArrays(A, m, B, n) << endl;
    return 0;
}
