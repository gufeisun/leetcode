struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x): val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *detectCycle(ListNode *head) {
        if (head == NULL) {
            return NULL;
        }
        ListNode *slow = head;
        ListNode *fast = head;
        while (slow != NULL && fast != NULL) {
            slow = slow->next;
            fast = fast->next;
            if (fast == NULL)
                return NULL;
            else fast = fast->next;
            if (fast == NULL)
                return NULL;
            if (slow == fast) break;
        }
        slow = head;
        while (slow != fast) {
            slow = slow->next;
            fast = fast->next;
        }
        return slow;
    }
};
