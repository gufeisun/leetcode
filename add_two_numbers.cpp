#include <string>
#include <strstream>
#include <iostream>
#include "math.h"

using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x): val(x), next(NULL) {}
};

class Solution {
public:
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
        int carry = 0;
        ListNode head(0);
        ListNode *iter_node = &head;
        ListNode *iter_l1 = l1;
        ListNode *iter_l2 = l2;
        while (iter_l1 && iter_l2) {
            int val = iter_l1->val + iter_l2->val + carry;
            carry = val / 10;
            ListNode *new_node = new ListNode(val % 10);
            iter_node->next = new_node;
            iter_node = new_node;

            iter_l1 = iter_l1->next;
            iter_l2 = iter_l2->next;
        }

        ListNode *remain = iter_l1 ? iter_l1 : iter_l2;
        while (remain) {
            int val = remain->val + carry;
            carry = val / 10;
            ListNode *new_node =  new ListNode(val % 10);
            iter_node->next = new_node;
            iter_node = new_node;
            remain = remain->next;
        }

        if (carry > 0) {
            ListNode *new_node =  new ListNode(carry);
            iter_node->next = new_node;
            iter_node = new_node;
        }

        return head.next;
    }
};


int main() {
    Solution s;
    ListNode *l1, *l2;
    ListNode a(1), b(3), c(7), d(2), e(2), f(5);
    l1 = &a;
    l1->next = &b;
    l1->next->next = &c;

    l2 = &d;
    l2->next = &e;
    l2->next->next = &f;

    ListNode *result = s.addTwoNumbers(l1, l2);

    return 0;
}
