#include <algorithm>
#include <vector>
using namespace std;
class Solution {
public:
    int maxArea(vector<int> &height) {
        int length = height.size();
        if (length <= 1) return 0;
        int i = 0;
        int j = length - 1;
        int max_area = min(height[i], height[j]) * (j - i);
        while (i < j) {
            if (height[i] < height[j])
                i++;
            else j--;
            int temp = min(height[i], height[j]) * (j - i);
            if (temp > max_area) max_area = temp;
        }
        return max_area;
    }
};
