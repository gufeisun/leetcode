#include <string>
#include <iostream>
using namespace std;

class Solution {
public:
    int reverse(int x) {
        int temp = 0, flag = 1;
        if (x < 0) {
            flag = -1;
            x = -x;
        }
        while(x) {
            temp*=10;
            temp+=x%10;
            x/=10;
        }
        return temp*flag;
    }
};


int main() {
    Solution s;
    int test = -1020;
    cout << s.reverse(test) << endl;
    return 0;
}
