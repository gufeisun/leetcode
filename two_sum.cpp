#include <vector>
#include <iostream>
using namespace std;

class Solution {
  public:
    vector<int> twoSum(vector<int> &numbers, int target) {
      vector<int> result;
      for (int index = 0; index < numbers.size(); ++index) {
        for (int index2 = index + 1; index2 < numbers.size(); ++index2) {
          if (numbers[index] + numbers[index2] == target) {
            result.push_back(index + 1);
            result.push_back(index2 + 1);
            return result;
          }
        }
      }
      return result;
    }
};


int main() {
  Solution s;
  int a[] = {0, 2, 1, 4, 7, 3};
  int target= 6;;
  vector<int> number(a, a+6);
  vector<int> result = s.twoSum(number, target);
  if (result.size() != 0) {
    cout<< result[0] << " " << result[1] <<endl;
  }
  return 0;
}
