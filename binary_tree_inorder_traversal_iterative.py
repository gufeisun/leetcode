class Solution:
    def inorderTraversal(self, root):
        result = []
        stack = []
        index = root
        #Why the following statement runs slower????
        #while (not index or not stack):
        while (index is not None or len(stack) != 0):
            while (index):
                stack.append(index)
                index = index.left
            if (stack):
                index = stack.pop()
                result.append(index.val)
                index = index.right
        return result
