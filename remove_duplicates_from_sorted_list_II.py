class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    def deleteDuplicates(self, head):
        if (head is None or head.next is None):
            return head
        dummy = ListNode(-1)
        iter_tail = dummy
        iter_tail.next = head
        iter_left = head
        iter_right = head.next
        flag = False

        while (iter_left and iter_right):
            while (iter_right and iter_left.val == iter_right.val):
                iter_right = iter_right.next
                flag = True
            if (flag is True):
                iter_tail.next = iter_right
            else:
                iter_tail = iter_tail.next

            flag = False
            iter_left = iter_right
            if iter_right:
                iter_right = iter_right.next
        return dummy.next

    def printNodeList(self, head):
        while (head):
            print head.val
            head = head.next


s = Solution()
head = ListNode(1)
head.next = ListNode(1)
head.next.next = ListNode(2)
head.next.next.next = ListNode(2)
head.next.next.next.next = ListNode(3)
head.next.next.next.next.next = ListNode(4)

m = s.deleteDuplicates(head)
s.printNodeList(m)
