class Solution:
    def detectCycle(self, head):
        if (head is None):
            return None
        slow_iter = head
        fast_iter = head
        while (slow_iter is not None and fast_iter is not None):
            slow_iter = slow_iter.next
            fast_iter = fast_iter.next
            if fast_iter is None:
                return None
            fast_iter = fast_iter.next
            if (fast_iter is None):
                return None
            if (slow_iter == fast_iter):
                break
        slow_iter = head
        while (slow_iter != fast_iter):
            slow_iter = slow_iter.next
            fast_iter = fast_iter.next
        return slow_iter
