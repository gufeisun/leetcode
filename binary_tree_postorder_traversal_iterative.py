class Solution:
    def postorderTraversal(self, root):
        result = []
        stack = []
        index = root
        pre = root
        while(index):
            while(index.left):
                stack.append(index)
                index = index.left
            while(index.right is None or index.right == pre):
                result.append(index.val)
                pre = index
                if stack:
                    return result
                index = stack.pop()
            stack.append(index)
            index = index.right
        return result
