class Solution:
    def searchInsert(self, A, target):
        left = 0
        length = len(A)
        right = length - 1
        while left <= right:
            mid = (left + right) / 2
            if A[mid] == target:
                return mid
            if A[mid] > target:
                right = mid - 1
            else:
                left = mid + 1
        return left
