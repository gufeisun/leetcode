class Solution {
public:
    bool isValidBST(TreeNode *root) {
        return isValidBST_Core(root, INT_MIN, INT_MAX);
    }

    bool isValidBST_Core(TreeNode *node, int MIN, int MAX) {
        if (node == NULL) {
            return true;
        }
        if (node->val > MIN && node->val < MAX && isValidBST_Core(node->left, MIN, node->val)
                && isValidBST_Core(node->right, node->val, MAX))
            return true;
        else
            return false;
    }
};
