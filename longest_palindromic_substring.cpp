#include <string>
#include <iostream>
using namespace std;

class Solution {
public:
    string longestPalindrome(string s) {
        int length = s.length();
        if (length == 0) {
            return "";
        }
        if (length == 1) {
            return s;
        }

        bool **dp = new bool*[length];
        for (int i = 0; i < length; ++i) {
            dp[i] = new bool[length];
        }

        for (int i = 0; i < length; ++i) {
            for (int j = 0; j < length; ++j) {
                if (i >= j)  {
                    dp[i][j] = true;
                } else {
                    dp[i][j]= false;
                }
            }
        }

        int begin = 0, end = 0, max_len = 1;
        for (int i = 1; i < length; ++i) {
            for (int j = 0; j + i < length; ++j) {
                int k = j + i;
                if (s[j] != s[k]) {
                    dp[j][k] = false;
                } else {
                    dp[j][k] = dp[j+1][k-1];
                    if (dp[j][k]) {
                        if (k + 1 > max_len) {
                            max_len = i + 1;
                            begin = j;
                            end = k;
                        }
                    }
                }
            }
        }

        for (int i = 0; i < length; ++i) {
            delete [] dp[i];
        }
        delete dp;

        return s.substr(begin, max_len);
    }
};


int main() {
    Solution s;
    string test = "abcdcbaddc";
    cout << s.longestPalindrome(test) << endl;
    return 0;
}
