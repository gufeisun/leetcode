int class Solution:
    def removeElement(self, A, elem):
        A[:] = [a for a in A if a != elem]
        return len(A)


s = Solution()
A = [4, 5, 6, 4]
s.removeElement(A, 4)
print type(A)
print A
print len(A)
