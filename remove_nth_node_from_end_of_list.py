class Solution:
    def removeNthFromEnd(self, head, n):
        slow = head
        fast = head
        step = 0
        for i in range(n):
            fast = fast.next
            step += 1
        if fast is None:
            head = head.next
            return head
        while (fast.next is not None):
            slow = slow.next
            fast = fast.next
        slow.next = slow.next.next
        return head
