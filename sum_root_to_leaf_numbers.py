class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution:
    def dfs(self, root, cur, res):
        if (root.left is None and root.right is None):
            cur = cur * 10 + root.val
            res["sum"] += cur
        else:
            cur = cur * 10 + root.val
            if (root.left is not None):
                self.dfs(root.left, cur, res)
            if (root.right is not None):
                self.dfs(root.right, cur, res)

    def sumNumbers(self, root):
        res = {"sum": 0}
        if (root is None):
            return res["sum"]
        self.dfs(root, 0, res)
        return res["sum"]
