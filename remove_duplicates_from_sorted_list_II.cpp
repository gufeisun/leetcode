struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x): val(x), next(NULL) {};
};

class Solution {
public:
    ListNode *deleteDuplicates(ListNode *head) {
        if (head == NULL || head->next == NULL) return head;
        ListNode dummy(-1);
        ListNode *tail = &dummy;
        tail->next = head;
        ListNode *left = head;
        ListNode *right = head->next;
        bool flag = false;

        while (left && right) {
            while (right && left->val == right->val) {
                right = right->next;
                flag = true;
            }
            if (flag == true) tail->next = right;
            else tail = tail->next;

            flag = false;
            left = right;
            if (right) right = right->next;
        }

        return dummy.next;
    }
};
