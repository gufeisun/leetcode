from sys import maxint


class Solution:
    def isValidBST(self, root):
        return self.isValidBST_R(root, (-maxint - 1), maxint)

    def isValidBST_R(self, node, MIN, MAX):
        if node is None:
            return True
        if (node.val > MIN and node.val < MAX
                and self.isValidBST_R(node.left, MIN, node.val)
                and self.isValidBST_R(node.right, MAX)):
            return True
        else:
            return False
