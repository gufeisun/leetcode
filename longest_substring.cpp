

#include <iostream>
#include <string>

using namespace std;
using std::string;

class Solution {
  public:
    int lengthOfLongestSubstring(string s) {
      int n = s.length();
      int i = 0, j = 0;
      int maxLen = 0;
      bool exist[256] = { false };
      while (j < n) {
        if (exist[s[j]]) {
          maxLen = max(maxLen, j-i);
          while (s[i] != s[j]) {
            exist[s[i]] = false;
            i++;
          }
          i++;
          j++;
        } else {
          exist[s[j]] = true;
          j++;
        }
      }
      maxLen = max(maxLen, n-i);
      return maxLen;
    }

    int lengthOfLongestSubstring2(string s) {
      int length = s.length();
      int count[256];
      memset(count, -1, sizeof(count));
      int len = 0;
      int max_len = 0;

      for (int i = 0; i < length; ++i, ++len) {
        if (count[s[i]] >= 0) {
          max_len = max(len, max_len);
          len = 0;
          i = count[s[i]] + 1;
          memset(count, -1, sizeof(count));
        }
        count[s[i]] = i;
      }

      return max(len, max_len);
    }

};


int main() {
  Solution s;

  string test_string = "abcagfbacad";
  cout << s.lengthOfLongestSubstring2(test_string) <<endl;

  return 0;
}

