#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
    void core(vector<int> &prices, vector<int> &left, vector<int> &right) {
        int length = prices.size();

        int min_value = prices[0];
        left[0] = 0;
        for (int i = 1; i < length; ++i) {
            left[i] = left[i-1];
            if (prices[i] - min_value > left[i-1]) {
                left[i] = prices[i] - min_value;
            }
            if (prices[i] < min_value)
                min_value = prices[i];
        }


        int max_value = prices[length - 1];
        right[length-1] = 0;
        for (int i = length - 2; i >= 0; i--) {
            right[i] = right[i+1];
            if (max_value - prices[i] > right[i+1]) {
                right[i] = max_value - prices[i];
            }
            if (prices[i] > max_value)
                max_value = prices[i];
        }

        for (int i = 0; i < length; ++i) {
            cout<< "left[" << i << "]:" << left[i] << endl;
        }

        for (int i = 0; i < length; ++i) {
            cout<< "right[" << i << "]:" << right[i] << endl;
        }
    }

    int maxProfit(vector<int> &prices) {
        int length = prices.size();
        if (length == 0)
            return 0;

        vector<int> left(length, 0);
        vector<int> right(length, 0);
        core(prices, left, right);

        int max_profit = 0;
        for (int i = 0; i < length; ++i) {
            max_profit = ((max_profit > left[i] + right[i]) ? max_profit : (left[i] + right[i]));
        }
        return max_profit;
    }
};

int main() {
    Solution s;
    int data[5] = {2, 1, 2, 0, 1};
    vector<int> prices(data, data + 5);

    cout<<s.maxProfit(prices)<<endl;
    return 0;
}
