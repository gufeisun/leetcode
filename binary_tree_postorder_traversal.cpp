class Solution {
    public:
        void postorder(TreeNode *root, vector<int> &result) {
            if (root->left) postorder(root->left, result);
            if (root->right) postorder(root->right, result);
            result.push_back(root->val);
        }
        vector<int> postorderTraversal(TreeNode *root) {
            vector<int> result;
            if (root)
                postorder(root, result);
            return result;
        }
};
