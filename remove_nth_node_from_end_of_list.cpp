class Solution {
public:
    ListNode *removeNthFromEnd(ListNode *head, int n) {
        ListNode *slow = head;
        ListNode *fast = head;
        int step = 0;
        while (step < n && fast != NULL) {
            fast = fast->next;
            step++;
        }
        if (step == n && fast == NULL) {
            head = head->next;
            delete slow;
            return head;
        }
        while (fast->next != NULL) {
            slow = slow->next;
            fast = fast->next;
        }
        slow->next = slow->next->next;
        return head;
    }
};
