#include <assert.h>
#include <iostream>
using namespace std;

class Solution {
public:
    int atoi(const char *str) {
        assert(str != NULL);
        long long result = 0;
        int sign = 1;
        while (*str == ' ') {
            ++str;
        }

        if (*str == '+') {
            ++str;
        } else if (*str == '-') {
            sign = -1;
            ++str;
        } else if (!isdigit(*str)) {
            return 0;
        }

        while (*str != '\0') {
            if (isdigit(*str)) {
                result = result * 10 + (*str - '0');
                if (result * sign > INT_MAX)
                    return INT_MAX;
                if (result * sign < INT_MIN)
                    return INT_MIN;
            } else {
                break;
            }
            ++str;
        }

        return result * sign;
    }
};


int main() {
    Solution s;
    char test[] = "2147983648";
    cout << s.atoi(test) << endl;
    return 0;
}
