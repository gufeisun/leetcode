#include <vector>
#include <iostream>
using namespace std;

class Solution {
public:
    bool isPalindrome(int x) {
        if (x < 0) return false;
        if (x == 0) return true;
        if (x < 10) return true;
        int length = 0;
        int x_for_length = x;
        while (x_for_length) {
            x_for_length /= 10;
            ++length;
        }

        int count = length / 2;
        int start, end;
        int start_x = x;
        for (int i = 1; i <= count; ++i) {
            start = start_x % 10;
            start_x /= 10;
            int end_x = x;
            for (int j = 1; j <= length + 1 - i; ++j) {
                end = end_x % 10;
                end_x /= 10;
            }
            if (start != end) {
                return false;
            }
        }
        return true;
    }
};


int main() {
    Solution s;
    int test = 9999;
    cout << (s.isPalindrome(test) ? "True" : "False") << endl;
    return 0;
}
