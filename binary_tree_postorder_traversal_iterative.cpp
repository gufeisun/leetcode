#include <stdio.h>
#include <vector>

using namespace std;
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x): val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    vector<int> postorderTraversal(TreeNode *root) {
        vector<int> result;
        TreeNode *index = root;
        TreeNode *pre = root;
        vector<TreeNode*> stack;
        while(index) {
            while(index->left) {
                stack.push_back(index);
                index = index->left;
            }

            // while there is no right node or right node has been outputed
            while(index->right == NULL || index->right == pre) {
                result.push_back(index->val);
                pre = index; // Record the previous output
                if (stack.size() == 0) return result;
                index = stack.back();
                stack.pop_back(); // The STD pop_back function of Vector doesn't return value.
            }

            // Deal with the right node
            stack.push_back(index);
            index = index->right;
        }
        return result;
    }
};


