class Solution:
    def hasCycle(self, head):
        if (head is None):
            return False
        slow_iter = head
        fast_iter = head.next
        while (slow_iter is not None and fast_iter is not None):
            if (slow_iter == fast_iter):
                return True
            slow_iter = slow_iter.next
            fast_iter = fast_iter.next
            if (fast_iter is None):
                return False
            fast_iter = fast_iter.next
        return False
